# Conta Digital

## Resumo

Conta digital empresarial com cartão VISA internacional (pré-pago).

## Condições e taxas

Todas as contas Astecas possuem mensalmente **5 TED / DOC** e **20 transferências** entre conta Astecas gratuitas. Após isso as taxas a seguir serão cobradas:

* **Mensalidade**: R$ 15,00
* **Emissão do cartão físico**: R$ 15,00
* **TED / DOC**: R$ 5,00
* **Saque (banco 24 horas, saque-pague e correios)**: R$ 6,50
* **Transferência entre contas Astecas**: R$ 0,10