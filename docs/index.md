# Gestão 360º

## O que é?
O **Gestão 360º** é uma solução completa de gestão financeira e empresarial. Temos soluções em:

* [Gestão do Fluxo de Caixa](gestao-financeira.md)
* [Sistema de PDV (Caixa) ](pdv.md)
* [Serviços Contábeis](contabilidade.md)
* [Máquina de Cartão](maquina-cartao.md)
* [Conta Digital](conta-digital.md)
* [Gestão de Estoque](pdv.md)
* [B.I.(Business Intelligence)](gestao-financeira.md)

## Diferencial

O **grande diferencial** é que **vendemos um serviço**, ou seja, “colocamos a mão na massa” junto com o cliente. 

## Foco da venda

A venda é focada em explicar ao cliente o porque ele pode ganhar mais dinheiro com os nossos serviços, e como nossa consultoria em gestão empresarial pode mudar a história da empresa. O que fazemos é um **serviço disruptivo**, pois usamos nossas plataformas e sistemas como **ferramentas de auxílio na prestação de serviço** e, atualmente, não tem nenhum prestador de serviço que ofereça tantas soluções em um único serviço. 

Para fechar o raciocínio, indicamos a você que deixe claro ao cliente que **não vendemos software, mas sim serviços que agregam valor a empresa**.
