# PDV

## Resumo

Sistema de frente de caixa **voltado para o comércio com gestão de estoque e produtos**. O PDV **funciona online e offline**. No pacote G360 está incluso 2 usuários.

## Benefícios

**`Gestão de Estoque`**
: Fazemos todas as analises cadastrais e tributárias dos produtos. Colaboramos para gestão do tempo de compra (tempo de ruptura) e disponibilizamos:
  * Relatório de produtos destaque de vendas
  * Relatório de custos
  * Tempo de produto parado no estoque
  * Todos os demais relatório tradicionais.

## Condições

PDV compatível com NFC-e e S@T

* **Implantação**: R$ 1.099,00
* **Mensalidade comércio (2 usuários)**: R$ 179,00
* **Usuário adicional**: R$ 27,00