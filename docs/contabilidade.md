# Contabilidade

## Resumo

Tenha a melhor consultoria contábil com a expertise de mais de 21 anos da MS Consulting para:

* Analisar cadastros de produtos
* Fazer seu planejamento tributário
* Folhas de pagamento
* Declaração anual
* Reduzir os seus impostos de forma legal
* Ter especialistas em resultados
* E muito mais...

## Condições

* Solicitar faturamento (vendas) do cliente para elaborar o preço.
* Perguntar ao cliente quantidade de funcionários para elaborar o preço.