# Máquina de Cartão

## Resumo

Aluguel ou venda de maquineta de cartão. 

???+ warning "Casos de reconciliação dos recebíveis"
    Caso pacote assinado seja G360 ou Gestão Financeira será realizado a conciliação dos recebíveis.

## Condições

* **Venda**: R$ 1.200,00
* **Aluguel**: R$ 127,00

???+ info "Maquineta G360"
    Caso o pacote assinado seja o G360, está inclusa uma máquina.

## Equipamento

<figure style="width: 350px;">
    <img src="../assets/images/maquininha.png" />
</figure>

## Taxas

**`Não antecipado`**
: Crédito D+30 e débito D+1.

**`Antecipado`**
: Crédito D+1 e débito D+1.

**`Bandeiras Aceitas`**
: * **Crédito no Chip**: VISA, MASTER, ELO, BANESCARD, HIPERCARD e HIPER.
  * **Crédito na Tarja**: AMEX, DINNERS, JCB e AURA.
  * **Crédito no Online**: VISA, MASTER, ELO, AMEX, DINNERS, JCB e AURA.
  * **Débito**: VISA, MASTER, ELO e BANESCARD.

## Planos

??? info "Plano padrão"
    * **Observações**:
      Plano para a maioria dos clientes.

    * **Não antecipado**:

    | Forma                         | Taxa  | Taxa Elo |
    | :---------------------------- | :---- | :------- |
    | Débito                        | 1.60% | 2.85%    |
    | Crédito à vista               | 2.50% | 3.75%    |
    | Crédito parcelado de 2x a 6x  | 3.43% | 4.87%    |
    | Crédito parcelado de 7x a 12x | 3.69% | 5.47%    |

    * **Antecipado**:
    
    | Forma                    | Taxa   | Taxa Elo |
    | :----------------------- | :----- | :------- |
    | Débito                   | 1.68%  | 2.93%    |
    | Crédito à vista          | 3.67%  | 4.91%    |
    | Crédito parcelado de 2x  | 5.19%  | 6.61%    |
    | Crédito parcelado de 3x  | 5.79%  | 7.20%    |
    | Crédito parcelado de 4x  | 6.39%  | 7.79%    |
    | Crédito parcelado de 5x  | 6.99%  | 8.39%    |
    | Crédito parcelado de 6x  | 7.61%  | 8.99%    |
    | Crédito parcelado de 7x  | 8.47%  | 10.17%   |
    | Crédito parcelado de 8x  | 9.09%  | 10.78%   |
    | Crédito parcelado de 9x  | 9.72%  | 11.39%   |
    | Crédito parcelado de 10x | 10.35% | 12.01%   |
    | Crédito parcelado de 11x | 10.98% | 12.63%   |
    | Crédito parcelado de 12x | 11.62% | 13.26%   |

??? info "Plano supermercado"
    * **Observações**:
      Plano com os seguintes CNAES:

    * **Não antecipado**:

    | Forma                         | Taxa  | Taxa Elo |
    | :---------------------------- | :---- | :------- |
    | Débito                        | 1.15% | 2.26%    |
    | Crédito à vista               | 2.14% | 3.41%    |
    | Crédito parcelado de 2x a 6x  | 3.24% | 4.51%    |
    | Crédito parcelado de 7x a 12x | 3.69% | 5.47%    |

    * **Antecipado**:

    | Forma                    | Taxa   | Taxa Elo |
    | :----------------------- | :----- | :------- |
    | Débito                   | 1.23%  | 2.34%    |
    | Crédito à vista          | 3.22%  | 4.48%    |
    | Crédito parcelado de 2x  | 5.00%  | 6.25%    |
    | Crédito parcelado de 3x  | 5.60%  | 6.84%    |
    | Crédito parcelado de 4x  | 6.20%  | 7.44%    |
    | Crédito parcelado de 5x  | 6.81%  | 8.04%    |
    | Crédito parcelado de 6x  | 7.42%  | 8.64%    |
    | Crédito parcelado de 7x  | 8.47%  | N/A      |
    | Crédito parcelado de 8x  | 9.09%  | N/A      |
    | Crédito parcelado de 9x  | 9.72%  | N/A      |
    | Crédito parcelado de 10x | 10.35% | N/A      |
    | Crédito parcelado de 11x | 10.98% | N/A      |
    | Crédito parcelado de 12x | 11.62% | N/A      |

??? info "Plano combustível"
    * **Observações**:
      Plano com os seguintes CNAES:

    * **Não antecipado**:

    | Forma                         | Taxa  | Taxa Elo |
    | :---------------------------- | :---- | :------- |
    | Débito                        | 1.40% | 2.61%    |
    | Crédito à vista               | 2.34% | 3.41%    |
    | Crédito parcelado de 2x a 6x  | 3.34% | 4.56%    |
    | Crédito parcelado de 7x a 12x | 3.69% | 5.47%    |

    * **Antecipado**:

    | Forma                    | Taxa   | Taxa Elo |
    | :----------------------- | :----- | :------- |
    | Débito                   | 1.47%  | 2.69%    |
    | Crédito à vista          | 3.52%  | 4.57%    |
    | Crédito parcelado de 2x  | 5.10%  | 6.30%    |
    | Crédito parcelado de 3x  | 5.70%  | 6.89%    |
    | Crédito parcelado de 4x  | 6.30%  | 7.49%    |
    | Crédito parcelado de 5x  | 6.91%  | 8.09%    |
    | Crédito parcelado de 6x  | 7.52%  | 8.69%    |
    | Crédito parcelado de 7x  | 8.47%  | 10.17%   |
    | Crédito parcelado de 8x  | 9.09%  | 10.78%   |
    | Crédito parcelado de 9x  | 9.72%  | 11.39%   |
    | Crédito parcelado de 10x | 10.35% | 12.01%   |
    | Crédito parcelado de 11x | 10.98% | 12.63%   |
    | Crédito parcelado de 12x | 11.62% | 13.26%   |

??? info "Plano ticketing"
    * **Observações**:
      Plano com os seguintes CNAES:

    * **Não antecipado**:

    | Forma                         | Taxa  | Taxa Elo |
    | :---------------------------- | :---- | :------- |
    | Débito                        | 1.47% | 2.70%    |
    | Crédito à vista               | 2.20% | 2.85%    |
    | Crédito parcelado de 2x a 6x  | 3.42% | 4.15%    |
    | Crédito parcelado de 7x a 12x | 3.64% | 4.50%    |

    * **Antecipado**:

    | Forma                    | Taxa   | Taxa Elo |
    | :----------------------- | :----- | :------- |
    | Débito                   | 0.00%  | 2.78%    |
    | Crédito à vista          | 3.38%  | 4.02%    |
    | Crédito parcelado de 2x  | 5.18%  | 5.21%    |
    | Crédito parcelado de 3x  | 5.78%  | 5.81%    |
    | Crédito parcelado de 4x  | 6.38%  | 6.41%    |
    | Crédito parcelado de 5x  | 6.98%  | 7.02%    |
    | Crédito parcelado de 6x  | 7.60%  | 7.63%    |
    | Crédito parcelado de 7x  | 8.43%  | 8.53%    |
    | Crédito parcelado de 8x  | 9.05%  | 9.15%    |
    | Crédito parcelado de 9x  | 9.67%  | 9.78%    |
    | Crédito parcelado de 10x | 10.30% | 10.41%   |
    | Crédito parcelado de 11x | 10.94% | 11.04%   |
    | Crédito parcelado de 12x | 11.58% | 11.68%   |

??? info "Plano alimentação"
    * **Observações**:
      Plano com os seguintes CNAES:

    * **Não antecipado**:

    | Forma                         | Taxa  | Taxa Elo |
    | :---------------------------- | :---- | :------- |
    | Débito                        | 1.36% | 1.90%    |
    | Crédito à vista               | 2.20% | 2.85%    |

    * **Antecipado**:

    | Forma                    | Taxa   | Taxa Elo |
    | :----------------------- | :----- | :------- |
    | Débito                   | 1.43%  | 1.98%    |
    | Crédito à vista          | 3.38%  | 4.02%    |