# Gestão Financeira

## Resumo

Através desse serviço incrível, o cliente tem **acesso a todos os recursos da plataforma digital Astecas**, tais como:

* Fluxo de Caixa Gerencial 100% Online
* Demonstrativo de Resultados Financeiros
* Análises Financeiras

Além disso são **disponibilizados consultores, para levantar problemas e soluções**, através de **atendimento personalizado**, com canais de comunicação por chat, e-mail, telefone e WhatsApp.

## B.I (Business Intelligence)

No painel de analise, chamado de **DashBoard**, disponibilizamos:

* Indicadores de Performance (KPIs)
* Gráficos de Análises Financeira do Negócio

### Relatórios disponíveis

* Fluxo de Caixa Mensal
* Fluxo de Caixa Diário
* Lançamentos Detalhados, contendo:
  * Centro de Custo
  * Unidade / Filial
  * Departamento
  * Fornecedor / Cliente
  * Conta Bancária
  * Colaborador
  * Caixa

## Pacote

O pacote contém:

* 200 documentos
* Contas a pagar e receber

### Condições

O valor é cobrado a cada **100 documentos** equivalente a R$ 420,00.

A partir de **200 documentos** será cobrado um valor proporcional a 100, por exemplo: se consumido 240 documentos, o valor cobrado será de 300 documentos.